const gulp = require('gulp');
const concat = require('gulp-concat');
const image = require('gulp-image');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const jquery = require('jquery');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const pug = require('gulp-pug');
const flatten = require('gulp-flatten');

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('dist/'));
});

gulp.task('fonts', function () {
    gulp.src('./src/**/fonts/**')
        .pipe(gulp.dest('./dist'));
});

gulp.task('images', function () {
    gulp.src('./src/**/images/*')
        .pipe(gulp.dest('./dist'));
});

gulp.task('sass', function () {
    return gulp.src('./src/styles/main/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
        }))
        .pipe(flatten())
        // .pipe(concat('style.css'))
        .pipe(gulp.dest('dist/styles'));
});

gulp.task('pug', function () {
    return gulp.src('./src/pages/**/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('./dist/pages'));
});

gulp.task('del', function () {
    return del.sync('dist');
});

gulp.task('reloader', function () {
    browserSync({
        server: {
            baseDir: './dist/'
        }
    });
});

gulp.task('watch', ['del', 'sass', 'html', 'reloader', 'images', 'fonts'], function () {
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/**/fonts/**', ['fonts']);
    gulp.watch('./src/**/images/*', ['images']);
    gulp.watch('./dist/**/*.html', browserSync.reload);
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./dist/styles/**/*.css').on('change', browserSync.reload);

});